package dailyexcersisesSelenium;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DQ04IRCTCDropdown {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", ".\\Drivers\\chromedriver.exe");
		//System.setProperty("webdriver.chrome.driver", ".\\Drivers\\chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in");
		driver.findElementByLinkText("REGISTER").click();//navigate to register page
		WebElement ddCountry = driver.findElementByXPath("//select[@class='form-control ng-pristine ng-valid ng-touched']");
		//ddCountry.click();
//	WebDriverWait wait=new WebDriverWait(driver,60);
	//wait.until(ExpectedConditions.visibilityOf(ddCountry));
	//	WebElement ddCountry = driver.findElementByXPath("//select[@class='form-control ng-pristine ng-valid ng-touched']");
		Select country=new Select(ddCountry);
		List<WebElement> countryOptions = country.getOptions();//get all the options from country dropdown
		int i=0;
		for(WebElement eachCountry:countryOptions)
		{
		String option=eachCountry.getText();
		if(option.startsWith("E"))
		{
			i++;
			if(i==2)
			{
				System.out.println(option);
				break;
			}
		}
				
	}
	}

}
