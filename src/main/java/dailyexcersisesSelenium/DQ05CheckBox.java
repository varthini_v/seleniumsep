package dailyexcersisesSelenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class DQ05CheckBox {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", ".\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leafground.com/pages/checkbox.html");
		if(driver.findElementByXPath("//label[contains(text(),'Confirm Selenium is checked')]/following-sibling::input").isSelected())
		{
			System.out.println("The check box \"Confirm Selenium is checked\" is selected");
		}
		
	}

}
