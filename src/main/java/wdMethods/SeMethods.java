package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.xml.ws.WebServiceException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				
				ChromeOptions op=new ChromeOptions();
				op.addArguments("--disable-notifications");
						 driver = new ChromeDriver(op);
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			testStep("pass", "The Browser "+browser+" Launched Successfully");
	
		} catch (WebDriverException e) {
			testStep("fail", "The Browser "+browser+" Launched Successfully");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		WebElement ele = null;
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext":return driver.findElementByLinkText(locValue);
		
			} 
			
		}
		 catch (NoSuchElementException e) {
			testStep("Fail", "The element is not located ");
			
		}
		return ele;
	}

	@Override
	public WebElement locateElement(String locValue) {
		
		WebElement ele = null;
		try {
			return driver.findElementById(locValue);
		} 
	 catch (NoSuchElementException e) {
		testStep("Fail", "The element "+ele+" is not located ");
	}
 return ele;

	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			testStep("Pass", "The data "+data+" is entered successfully");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			testStep("Fail", "The data "+data+"has not entered");
		}
		finally
		{
			takeSnap();
		}

		
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			testStep("Pass", "The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			testStep("Fail", "The Element "+ele+" is not  Clicked ");
		}
		finally
		{
			takeSnap();
		}

	}
	
	public void clickWithoutSnap(WebElement ele) {
		try {
			ele.click();
			//testStep("Pass", "The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			//testStep("Fail", "The Element "+ele+"  is not Clicked");
		}
		
		
	}

	@Override
	public String getText(WebElement ele) {
		String text=null;
		try {
			 text = ele.getText();
			 testStep("Pass", "The text in the  element"+ele+"  has been read");
			 
		} catch (WebDriverException e) {
			testStep("Fail", "The text in the  element"+ele+" has not been read");
		}
		return text;


	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			testStep("Pass", "The DropDown Is Selected with "+value);
		} catch (WebDriverException e) {
			testStep("Fail", "The text "+value+" is not selected in the dropdown");
		}
		finally
		{
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd= new Select(ele);
			dd.selectByIndex(index);
			testStep("Pass", "The DropDown Is Selected with index"+index);
		} catch (WebDriverException e) {
			testStep("Fail", "The index  "+index+" is not selected in the dropdown");
		}
		finally
		{
			takeSnap();
		}
	}

	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByValue(value);
			testStep("Pass", "The DropDown Is Selected with "+value);
		} catch (WebDriverException e) {
			testStep("Fail", "The value  "+value+" is not selected in the dropdown");
		}
		finally
		{
			takeSnap();
		}

	}
	@Override
	public boolean verifyTitle(String expectedTitle) {
		String title=null;
		try {
			title=driver.getTitle();
			if(title.compareTo(expectedTitle)==0)
			{
				testStep("Pass", "The title matches with "+expectedTitle);
				return true;
			}
			else
			{
				testStep("Fail", "The title  does not matches with "+expectedTitle);
				return false;
			}
			
		}
			
		catch (WebDriverException e) {
			testStep("Fail", "The page title has not been read");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String actualtext = ele.getText();
			if(actualtext.compareTo(expectedText)==0)
			{
				testStep("Pass", "The text "+expectedText+"  matches exctly");
			}
			else
			{
				testStep("Fail", "The text "+expectedText+"  does not matches exctly");
			}
				
		} catch (Exception e) {
			testStep("Fail", "The text of the element "+ele+" has not been verified");
		}
		finally
		{
			takeSnap();
		}

		}
		


	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String actualtext = ele.getText();
			if(actualtext.contains(expectedText))
			
				{
					testStep("Pass", "The text "+expectedText+"  matches partially");
				}
				else
				{
					testStep("Fail", "The text "+expectedText+"  does not matche");
				}
					
			} catch (Exception e) {
				testStep("Fail", "The text of the element "+ele+" has not been verified");
			}
			finally
			{
				takeSnap();
			}

		}

	

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value)
	{
		try {
			String actualValue = ele.getAttribute(attribute);
			if(actualValue.compareTo(value)==0)
				{
				
					testStep("Pass", "The value of the attribute "+attribute+"  matches exctly with "+value);
				}
				else
				{
					testStep("Fail", "The value of the attribute "+attribute+"   does not matches with"+value);
				}
					
			} 
			catch (Exception e) 
			{
				testStep("Fail", "The value of the attribute "+attribute+" has not been read");
			}
			finally
			{
				takeSnap();
			}

		}
	public boolean verifyPartialAttributeOfTheElement(WebElement ele, String attribute, String value) {
		try {
			String actualValue = ele.getAttribute(attribute);
			if(actualValue.contains(value))
			{
				//testStep("Pass", "The value of the attribute "+attribute+"  matches exctly with "+value);
				return true;
			}
			else
			{
				//testStep("Fail", "The value of the attribute "+attribute+"   does not matches with"+value);
				return false;
			}
				
		} 
		catch (Exception e) 
		{
			testStep("Fail", "The value of the attribute "+attribute+" has not been read");
		}
		finally
		{
			takeSnap();
		}
		return false;
	}
	

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String actualValue = ele.getAttribute(attribute);
			if(actualValue.contains(value))
			{
				testStep("Pass", "The value of the attribute "+attribute+"  matches exctly with "+value);
				
			}
			else
			{
				testStep("Fail", "The value of the attribute "+attribute+"   does not matches with"+value);
				
			}
				
		} 
		catch (Exception e) 
		{
			testStep("Fail", "The value of the attribute "+attribute+" has not been read");
		}
		finally
		{
			takeSnap();
		}
	
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected())
			{
				testStep("Pass", "The Element "+ele+" has been selected");
			}
			else
			{
				testStep("Fail", "The element "+ele+" is  not selected");
			}
		} catch (WebDriverException e) {
			testStep("Fail", "The element "+ele+" is  not selected");
		}
		finally
		{
			takeSnap();
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed())
			{
				testStep("Pass", "The Element "+ele+" has been displayed");
			}
			else
			{
				testStep("Fail", "The element "+ele+" has not been displayed");
			}
		} catch (WebDriverException e) {
			testStep("Fail", "The element "+ele+" is  not displayed");
		}
		finally
		{
			takeSnap();
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> listOfWindow = new ArrayList<String>();
			listOfWindow.addAll(allWindows);
			System.out.println(listOfWindow);
			driver.switchTo().window(listOfWindow.get(index));
			testStep("Pass", "The Window is Switched ");
		} catch (WebDriverException e) {
			testStep("Fail", "No such window is present");
		}
		finally
		{
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			testStep("Pass", "Switched to the frame "+ele);
		} catch (WebDriverException e) {
			testStep("Fail", "No such window is present");
		}
		finally
		{
			takeSnap();
		}
	}


	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			testStep("Pass", "The alert has been accepted");
		} catch (WebDriverException e) {
			testStep("Fail", "No Alert is present");

		}
		finally
		{
			takeSnap();
		}
	}
	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			testStep("Pass", "The alert has been dismissed");
		} catch (WebDriverException e) {
			testStep("Fail", "No Alert is present");
		}
		finally
		{
			takeSnap();
		}

	}

	@Override
	public String getAlertText() {
		String text="";
	try {
		text = driver.switchTo().alert().getText();	
		testStep("Pass", "The test has been read from the alert");
		return text;
	} catch (WebServiceException e) {
		testStep("Fail", "No Alert is present");
	}
	finally
	{
		takeSnap();
	}
	return text;
	}

	//to get the number of rows in a table
	public int getTableRecordCount(WebElement ele)
	
	{ 
		int recordCount=0;
		
		try {
			List<WebElement> tableRows = ele.findElements(By.tagName("tr"));
			recordCount=tableRows.size();
			testStep("pass", "The records in the table are read");
			return recordCount;
			
		} catch (WebDriverException e) {
			testStep("Fail", "Table not found");
		}
		finally
		{
			takeSnap();
		}
		return recordCount;
	}
	
public WebElement locateDataInTable(WebElement ele,int row,int coloumn)
	
	{ 
	WebElement selectedColoumn=null;
		
		try {
			List<WebElement> tableRows = ele.findElements(By.tagName("tr"));
			if(tableRows.size()>0)
			{
				WebElement selectedRow = tableRows.get(row);
				List<WebElement> rows = selectedRow.findElements(By.tagName("td"));
				selectedColoumn = rows.get(coloumn);
				testStep("Pass", "Table data is located");
				return selectedColoumn;
			}
			
			
		} catch (WebDriverException e) {
			testStep("Fail", "Table not found");
		}
		finally
		{
			takeSnap();
		}
		return selectedColoumn;
	}
	
	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
	//	testStep("Pass", "The ScreenShot has been saved in the path successfully");
		} catch (IOException e) {
			testStep("Fail", "The file path/image does not exists");
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
			testStep("Pass", "The current browser session has been closed");
			driver.close();
			
		} catch (Exception e) {
			testStep("Fail", "Current browser session is  not availabe to be closed");
		}

	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
			testStep("Pass", "All browser sessions have been closed");
		} catch (Exception e) {
			testStep("Fail", "No browser session is available to br closed");
		}

	}


	

}
