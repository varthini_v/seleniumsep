package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ReadExcel;

public class ProjectMethods extends SeMethods {
	
	public static Object[][] dataArray;

	
	
	@BeforeSuite(groups= {"any"})
	public void beforeSiute(){
		startReport();
	}
	
	
	@Parameters({"browser","url"})	
	@BeforeMethod(groups= {"any"})
	public void login(String browser,String url) {
		reportName();
		startApp(browser, url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);*/
	}
	
	@AfterMethod(groups= {"any"})
	public void closeCurrentBrowser() {
		driver.close();
	}

	@AfterSuite(groups= {"any"})
	public void afterTest(){
		endResult();
	}
	
	@DataProvider(name="leaftaps",indices= {0})
	public Object[][] fetchdata() throws IOException
	{	
		
		ReadExcel read=new ReadExcel();
		dataArray=read.readData("./data/"+workBook+".xlsx",dataSheet);
		
		return dataArray;
		
		
	}

}
