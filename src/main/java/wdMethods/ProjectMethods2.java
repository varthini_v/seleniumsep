package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class ProjectMethods2 extends SeMethodsold{
	
	 public String testCaseName;
	public String testCaseDesc;
	public 	String author;
	public String category;
	
	@BeforeSuite
	public void beforeSiute(){
		startReport();
	}
	
	
	@BeforeMethod
	public void NavigateToStartPage()
	{   
		reportName(testCaseName, testCaseDesc, author, category);
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement stratJourney = locateElement("linktext", "Start your wonderful journey");
		click(stratJourney);
		
	}
	
	
	
	@AfterMethod
	public void closeCurrentBrowser() {
		closeBrowser();
	}

	@AfterSuite
	public void afterTest(){
		endResult();
	}

}
