package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeads {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".//Drivers//chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> windowsList=new ArrayList<>();
		windowsList.addAll(allWindows);
		driver.switchTo().window(windowsList.get(1));
		driver.manage().window().maximize();
		driver.findElementByName("firstName").sendKeys("Revathy");
		driver.findElementByClassName("x-btn-text").click();
		Thread.sleep(3000);
		String leadID = driver.findElementByXPath("(((//table[@class='x-grid3-row-table'])[1]//tr)[1]//td)[1]//a").getText();
		driver.findElementByXPath("(((//table[@class='x-grid3-row-table'])[1]//tr)[1]//td)[1]//a").click();
		driver.switchTo().window(windowsList.get(0));
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> allWindows1 = driver.getWindowHandles();
		List<String> windowsList1=new ArrayList<>();
		windowsList1.addAll(allWindows1);
		driver.switchTo().window(windowsList1.get(1));
		driver.manage().window().maximize();
		driver.findElementByName("firstName").sendKeys("didi");
		driver.findElementByClassName("x-btn-text").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(((//table[@class='x-grid3-row-table'])[1]//tr)[1]//td)[1]//a").click();
		driver.switchTo().window(windowsList.get(0));
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys(leadID);
		driver.findElementByClassName("x-btn-text").click();
		Set<String> allWindows2 = driver.getWindowHandles();
		List<String> windowsList2=new ArrayList<>();
		windowsList2.addAll(allWindows2);
		driver.switchTo().window(windowsList2.get(0));
		String errorMessage=driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		if(errorMessage.compareTo("No records to display")==0)
		{
			System.out.println("Pass");
		}
		
	}}
