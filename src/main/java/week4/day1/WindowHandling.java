package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver",".\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> windowList=new ArrayList<>();
		windowList.addAll(allWindows);
		driver.switchTo().window(windowList.get(1));
		System.out.println("Title of the new window is: "+driver.getTitle());
		File screenshot = driver.getScreenshotAs(OutputType.FILE);
		File fileObj=new File(".\\SnapShot\\Screenshot1.jpeg");
		FileUtils.copyFile(screenshot, fileObj);
		String secondWindow = driver.getWindowHandle();
		for(String eachWindow:allWindows)
		{
			if(eachWindow.compareTo(secondWindow)==0) 
			{
				driver.close();
				System.out.println("\"Contact us\" window is closed");
			}
		}
		
		

	}

}
