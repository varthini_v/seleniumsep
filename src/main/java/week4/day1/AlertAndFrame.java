package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertAndFrame {

	public static void main(String[] args) 
	{
		String Name="Varthini";
		System.setProperty("webdriver.chrome.driver",".\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert enterName = driver.switchTo().alert();
		enterName.sendKeys(Name);
		enterName.accept();
		String displayText = driver.findElementById("demo").getText();
		if(displayText.contains(Name))
		{
			System.out.println("Pass");
		}
	}

}
