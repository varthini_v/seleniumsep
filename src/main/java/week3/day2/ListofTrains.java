package week3.day2;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ListofTrains {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		driver.findElementById("buttonFromTo").click();
		if(driver.findElementById("chkSelectDateOnly").isSelected())
		{
			driver.findElementById("chkSelectDateOnly").click();
		}
		Thread.sleep(2000);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> trains = table.findElements(By.tagName("tr"));
		System.out.println(trains.size());
		
		for (int i = 0; i <trains.size()-1;i++) 
		{
			List<WebElement> trainDetails = trains.get(i).findElements(By.tagName("td"));
			System.out.println(trainDetails.get(1).getText());
		
		}

	}

}
