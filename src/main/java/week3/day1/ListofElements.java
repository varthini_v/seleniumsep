package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ListofElements {

	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	List<WebElement> links = driver.findElementsByTagName("a");
	System.out.println("Number of links in the page:"+links.size());
	links.get(3).click();
	System.out.println(links.get(4).getText());
	links.get(4).click();
	}

}
