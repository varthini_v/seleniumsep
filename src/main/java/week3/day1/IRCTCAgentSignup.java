package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTCAgentSignup {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",".\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("Varthini");
		driver.findElementById("userRegistrationForm:password").sendKeys("testpwd..77");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("testpwd..77");
		WebElement securityQuestion = driver.findElementById("userRegistrationForm:securityQ");
		Select ddSecurityQuestion=new Select(securityQuestion);
		ddSecurityQuestion.selectByValue("1");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("JAI Matric School");
		WebElement preferredLanguage = driver.findElementById("userRegistrationForm:prelan");
		Select ddPreferredLanguage=new Select(preferredLanguage);
		ddPreferredLanguage.selectByValue("en");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Varthini");
		List<WebElement> gender = driver.findElementsByName("userRegistrationForm:gender");
		gender.get(1).click();
		List<WebElement> martialStatus = driver.findElementsByName("userRegistrationForm:maritalStatus");
		martialStatus.get(0).click();
		WebElement dobDay = driver.findElementById("userRegistrationForm:dobDay");
		Select ddDOBDay=new Select(dobDay);
		ddDOBDay.selectByValue("26");
		WebElement dobMonth = driver.findElementById("userRegistrationForm:dobMonth");
		Select ddDOBMonth=new Select(dobMonth);
		ddDOBMonth.selectByValue("02");
		WebElement dobYear = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select ddDOBYear=new Select(dobYear);
		ddDOBYear.selectByValue("1992");
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select ddOccupation=new Select(occupation);
		ddOccupation.selectByValue("2");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select ddCountry=new Select(country);
		ddCountry.selectByValue("94");
		Thread.sleep(2000);
		driver.findElementById("userRegistrationForm:email").sendKeys("Varthini.pv@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("7373404766");
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select ddNationality=new Select(nationality);
		ddNationality.selectByValue("94");
		//Thread.sleep(2000);
		driver.findElementById("userRegistrationForm:address").sendKeys("Plot No d13");
		driver.findElementById("userRegistrationForm:street").sendKeys("Phase2 TNHB Colony");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("625008",Keys.TAB);
		Thread.sleep(2000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select ddCity=new Select(city);
		ddCity.selectByIndex(1);
		Thread.sleep(2000);
		WebElement postoffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select ddPostOffice=new Select(postoffice);
		ddPostOffice.selectByIndex(1);
		driver.findElementById("userRegistrationForm:landline").sendKeys("0452246247");
		driver.findElementById("userRegistrationForm:resAndOff").click();

		
		

	}

}