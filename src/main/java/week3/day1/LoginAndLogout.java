package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginAndLogout {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","E:\\Test Leaf\\Workspace\\Selenuim\\Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("ABC Company");
		driver.findElementById("createLeadForm_firstName").sendKeys("TestFirst");
		driver.findElementById("createLeadForm_lastName").sendKeys("TestLast");
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd_source=new Select(source);
		dd_source.selectByVisibleText("Public Relations");
		WebElement campaign = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd_campaign=new Select(campaign);
		dd_campaign.selectByValue("CATRQ_AUTOMOBILE");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select dd_industry=new Select(industry);
		List<WebElement> industriesOption = dd_industry.getOptions();
		for(WebElement eachIndustry:industriesOption)
		{
			 String industryText=eachIndustry.getText();
			if(industryText.startsWith("M"))
			{
				System.out.println(industryText);
			}
		}
	driver.findElementByName("submitButton").click();		
	}
}
