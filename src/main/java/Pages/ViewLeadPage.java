package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
	public ViewLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement eleFirstName;
	@FindBy(how=How.CLASS_NAME,using="subMenuButtonDangerous") WebElement eleDelete;
	
	@Then("verify lead is created as (.*)")
	public ViewLeadPage verifyFirstName(String data)
	{
		verifyExactText(eleFirstName, data);
		return this;
	}
	
	public MyLeadsPage clickDelete()
	{
		click(eleDelete);
		return new MyLeadsPage();
	}
	
}
