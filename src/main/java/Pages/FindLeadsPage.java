package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]" ) WebElement elefirstNameforFindLead;
	@FindBy(how=How.XPATH,using="//input[@name='firstName']" ) WebElement elefirstNameforMergeLead;
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how=How.XPATH,using="((//div[@class='x-grid3-viewport']//tr)[2]/td)[1]//a") WebElement eleTableData;
	@FindBy(how=How.XPATH,using="//div[@class='x-grid3-header-offset']//table") WebElement eleTable;
	
	public FindLeadsPage enterFirstNameforMergeLead(String data)
	{
		type(elefirstNameforMergeLead,data);
		return this;
	}
	public FindLeadsPage enterFirstNameforFindLead(String data)
	{
		type(elefirstNameforFindLead,data);
		return this;
	}
	
	public FindLeadsPage clickFindLeads()
	{
		click(eleFindLeads);
		return this;
	}
	
	public FindLeadsPage clicktTableDataInMergeLead()
	{
		clickWithoutSnap(eleTableData);	
		return this;
	}
	public ViewLeadPage clicktTableData()
	{
		clickWithoutSnap(eleTableData);	
		return new ViewLeadPage();
	}
	
	public FindLeadsPage getTableCount()
	{
		getTableRecordCount(eleTable);
		return this;
	}
	
	public String captureID()
	{
		String ID=getText(eleTableData);
		return ID;
	}
	
	public MergeLeadPage switchToMegreLeadPage()
	{
		switchToWindow(0);
		return new MergeLeadPage();
	}
	
	
}
