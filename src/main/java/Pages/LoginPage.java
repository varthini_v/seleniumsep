package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="username") WebElement eleUserName;
	@FindBy(how=How.ID,using="password") WebElement elePassword;
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
	
	@Given("Enter the username as (.*)")
	public LoginPage enterUserName(String userNameData)
	{
		type(eleUserName, userNameData);
		return this;
	}
	
	@Given("Enter the password as (.*)")
	public LoginPage enterPassword(String passwordData)
	{
		type(elePassword, passwordData);
		return this;
	}
	
	@Given("Click on Login")
	public HomePage clickLogin()
	{
		click(eleLogin);
		return new HomePage();
	}

}
