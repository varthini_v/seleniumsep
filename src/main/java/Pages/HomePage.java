package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA") WebElement eleCrmsfa;
	
	@Given("Click on crmsfa link")
	public MyHomePage clickCRMSFA()
	{
		click(eleCrmsfa);
		return new MyHomePage();
	}
	
	


}
