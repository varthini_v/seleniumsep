package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID,using="createLeadForm_primaryEmail") WebElement eleEmailID;
	@FindBy(how=How.ID,using="createLeadForm_primaryPhoneNumber") WebElement elePhoneNumber;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleSubmit;
	
	
	@Given("Enter firstname as (.*)")
	public CreateLeadPage enterFirstName(String data)
	{
		type(eleFirstName, data);
		return this;
	}
	
	@Given("Enter lastname as (.*)")
	public CreateLeadPage enterLastName(String data)
	{
		type(eleLastName, data);
		return this;
	}
	@Given("Enter companyname as (.*)")
	public CreateLeadPage enterCompanyName(String data)
	{
		type(eleCompanyName, data);
		return this;
	}
	
	/*public CreateLeadPage enterEmailID(String data)
	{
		type(eleEmailID, data);
		return this;
	}
	
	public CreateLeadPage enterPhoneNumber(String data)
	{
		type(elePhoneNumber, data);
		return this;
	}*/
	
	@When("Click on createlead")
	public ViewLeadPage clickLogin()
	{
		click(eleSubmit);
		return new ViewLeadPage();
	}

}
