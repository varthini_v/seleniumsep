package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {
	
	public MergeLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="(//img[@src='/images/fieldlookup.gif'])[1]") WebElement eleFirstIcon;
	@FindBy(how=How.XPATH,using="(//img[@src='/images/fieldlookup.gif'])[2]") WebElement eleSecondIcon;
	
	@FindBy(how=How.CLASS_NAME,using="buttonDangerous") WebElement eleMergeLead;
	@FindBy(how=How.XPATH,using="((//div[@class='x-grid3-viewport']//tr)[2]/td)[1]//a") WebElement eleTableData;
	

	
	public FindLeadsPage clickFirstIcon()
	{
		click(eleFirstIcon);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	public FindLeadsPage clickSecondIcon()
	{
		click(eleSecondIcon);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	public MergeLeadPage clickMergeLead()
	{
		clickWithoutSnap(eleMergeLead);
		return this;
	}
	public ViewLeadPage acceptMergeLeadAlert()
	{
		acceptAlert();
		return new ViewLeadPage();
	}
}
