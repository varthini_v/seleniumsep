package testCases2;


import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods2;

public class TC002FBLikesAndReviews extends ProjectMethods2 {
	
	@BeforeTest
	public void setTestCaseName()
	{
		testCaseName="TC002FBLikesandReviews";
		testCaseDesc="Get the number of Reviews";
		author="varthini";
		category="Smoke";
	}
	
	@Test
	public void likesandReviews()
	{
	
	startApp("Chrome","https://www.facebook.com/");
	WebElement email = locateElement("id","email");
	type(email, "7373404766");
	WebElement password = locateElement("id", "pass");
	type(password,"26feb1992");
	WebElement loginButton = locateElement("id", "loginbutton");
	click(loginButton);
	WebElement searchbar = locateElement("xpath", "//input[@class='_1frb']");
	click(searchbar);
	type(searchbar, "testleaf");
	WebElement searchIcon = locateElement("xpath", "//div[@class='_585-']//button[@type='submit']");
	click(searchIcon);
	WebElement likebutton = locateElement("xpath", "(//div[@class='_3ko9'])[1]//button[@value='1']");
	if(verifyPartialAttributeOfTheElement(likebutton, "class", "PageLikedButton")==false)
	{
		click(likebutton);
		verifyPartialAttribute(likebutton,"class", "PageLikedButton");
	}
	WebElement pageName = locateElement("xpath", "//div[text()='TestLeaf']");
	click(pageName);
	List<WebElement> reviews = driver.findElementsByXPath("//div[@class='_3dy3']");
	System.out.println("Number of Reviews: "+reviews.size());
	
	}

}
