package testCases2;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods2;

public class ZoomCar extends ProjectMethods2 {
	
@BeforeTest
public void setTestCaseName()
{
	testCaseName="ZoomCar";
	testCaseDesc="book a car";
	author="varthini";
	category="Smoke";
}
	

@Test(alwaysRun=true)
public void zookCar() throws InterruptedException
{
	startApp("chrome", "https://www.zoomcar.com/chennai");
	WebElement stratJourney = locateElement("linktext", "Start your wonderful journey");
	click(stratJourney);
	WebElement pickupPoints=locateElement("xpath", "(//div[@class='component-popular-locations']/div[@class='items'])[2]");
	click(pickupPoints);
	WebElement nextButton = locateElement("class", "proceed");
	click(nextButton);
    WebElement startDate = locateElement("xpath", "(//div[@class='day picked ']/following::div)[1]");
    String tomorrowDate=getText(startDate);
    click(startDate);
    //Thread.sleep(2000);
	click(nextButton);
	//Thread.sleep(3000);
	
	WebElement selectedDate = locateElement("xpath", "//div[@class='day picked ']");
	verifyExactText(selectedDate, tomorrowDate);
	click(nextButton);
	List<WebElement> cars = driver.findElementsByXPath("//div[@class='car-item']//div[@class='price']");
	List <String> price=new ArrayList<>();
	for(WebElement eachCar:cars)
	{
		String Temp=eachCar.getText();
		price.add(Temp);
	}
	String maxprice=Collections.max(price);
	System.out.println("number of cars listed: "+cars.size());
	String 	carprice=maxprice.substring(2, 5);
	System.out.println("maxprice "+carprice);
	WebElement brand = locateElement("xpath", "//div[contains(text(),'"+carprice+"')]/preceding::h3[1]");
	System.out.println("car with maximum price: "+brand.getText());
	WebElement bookNowButton = locateElement("xpath", "//div[contains(text(),'"+carprice+"')]/following-sibling::button");
	click(bookNowButton);

}
	
}

	
	
	