package week2.day2;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LearnSet {

	public static void main(String[] args) {
		Set<Integer> numbers=new LinkedHashSet<>();
		numbers.add(10);
		numbers.add(11);
		numbers.add(12);
		numbers.add(13);
		numbers.add(14);
		numbers.add(15);
		numbers.add(16);
		numbers.add(17);
		List<Integer> newnumbers=new ArrayList<>();
		newnumbers.addAll(numbers);
		
		for(int eachNumber:numbers)
		{
			System.out.print(eachNumber+" ");
		}
		System.out.println(" ");
		for(int i=0;i<newnumbers.size();i++)
		{
			if(i%2!=0)
			{
				System.out.print(newnumbers.get(i)+" ");
			}
		}

	}

}
