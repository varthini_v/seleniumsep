package week2.day2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class LearnMap {

	public static void main(String[] args) {
	String word="cognizant";
	int occurance=1;
	char[] arrayOfLetters=word.toCharArray();
	Map<Character,Integer> letters=new HashMap<>();
	/*for(int i=0;i<arrayOfLetters.length;i++)
	{
		if(letters.containsKey(arrayOfLetters[i]))
		{	
			occurance=letters.get(arrayOfLetters[i]);
			letters.put(arrayOfLetters[i], occurance+1);
		}
		else
		{
			letters.put(arrayOfLetters[i], occurance);
		}
			
	}*/
	
	for(char eachCharacter: arrayOfLetters)
	{
		if(letters.containsKey(eachCharacter))
		{	
			occurance=letters.get(eachCharacter);
			letters.put(eachCharacter, occurance+1);
		}
		else
		{
			letters.put(eachCharacter, occurance);
		}
			
	}
	for(Entry<Character,Integer> eachLetter:letters.entrySet())
	{
		System.out.println(eachLetter.getKey()+"--->"+eachLetter.getValue());
	}
	
	}

}
