package week2.day2;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class LearnMap2 {

	public static void main(String[] args) {
		Map<String,Integer> mobileList=new TreeMap<>();
		mobileList.put("Apple",75000);
		mobileList.put("Vivo", 30000);
		mobileList.put("OnePlus", 75000);
		mobileList.put("Samsung", 50000);
		mobileList.put("lenevo", 15000);
		
		System.out.println("Mobile List: \n"+mobileList+"\n");
		int max=0;
		for(Entry<String,Integer> eachMobile:mobileList.entrySet())
		{
			if(eachMobile.getValue()>max)
			{
				max=eachMobile.getValue();
			}
		}
		System.out.println("The mobile with highest Price\n");
		for(Entry<String,Integer> eachMobile:mobileList.entrySet())
		{
			if(eachMobile.getValue()==max)
			{
				System.out.println("MobileBrand: "+eachMobile.getKey()+" Price: "+eachMobile.getValue());
			}
		}
		
		int min=0;
		for(Entry<String,Integer> eachMobile:mobileList.entrySet())
		{
			if(eachMobile.getValue()<min)
			{
				min=eachMobile.getValue();
			}
		}
		System.out.println("The mobile with Lowest Price\n");
		System.out.println(min);
		
		for(Entry<String,Integer> eachMobile:mobileList.entrySet())
		{
			if(eachMobile.getValue()==min)
			{
				System.out.println("MobileBrand: "+eachMobile.getKey()+" Price: "+eachMobile.getValue());
			}
		}
	
	}

}
