package week2.day1;

public class SmartTelevision extends Television {
	
	public void connectToInternet()
	{
		System.out.println("Connects to Internet");
	}
	public void record()
	{
		System.out.println("Records the Program");
	}
	public void installApps()
	{
		System.out.println("Installs the selected Applications");
	}
	
	public static void main(String[] args) {
		
		Television tv=new Television();
		SmartTelevision sTv=new SmartTelevision();
		System.out.println("Features of Television");
		System.out.println("----------------------");
		tv.displayChannels();
		tv.changeChannels();
		tv.adjustVolume();
		tv.displaySettings();
		tv.changeSettings();
		tv.playdvd();
		System.out.println("");
		System.out.println("Features of SmartTV");
		System.out.println("-------------------");
		sTv.displayChannels();
		sTv.changeChannels();
		sTv.adjustVolume();
		sTv.displaySettings();
		sTv.changeSettings();
		sTv.playdvd();
		sTv.connectToInternet();
		sTv.record();
		sTv.installApps();
		System.out.println(" ");
		System.out.println("SmartTV is preferred due to More features");
		
	}

}
