package week2.day1;

public interface Entertainment {
	
	public void displayChannels();
	public void changeChannels();
	public void adjustVolume();

}
