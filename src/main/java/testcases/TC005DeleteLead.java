package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005DeleteLead extends ProjectMethods {
	
	@BeforeTest
	public void beforeTest()
	{
		reportName("TC005", "Delete a  Lead", "Varthini", "Smoke");
	}
	
	@Test
	public void DeleteLead()
	{

		WebElement eleCrmsfaLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmsfaLink);
		WebElement eleCreateLeadLink = locateElement("linktext", "Create Lead");
		click(eleCreateLeadLink);
		WebElement eleCompanyName = locateElement("createLeadForm_companyName");
		type(eleCompanyName, "Test0710 Company");
		WebElement eleFirstName = locateElement("createLeadForm_firstName");
		type(eleFirstName, "Test0710 varthini");
		WebElement eleLastName = locateElement("createLeadForm_lastName");
		type(eleLastName, "Test0710 k");
		/*WebElement eleIcon = locateElement("xpath", "//img[@src='/images/fieldlookup.gif']");
		click(eleIcon);
		switchToWindow(1);
		WebElement eleAssociateID = locateElement("xpath", "(((//table[@class='x-grid3-row-table'])[1]//tr)[1]/td)[1]//a");
		clickWithoutSnap(eleAssociateID);
		switchToWindow(0);*/
		WebElement eleDDIndustry = locateElement("createLeadForm_industryEnumId");
		selectDropDownUsingValue(eleDDIndustry, "IND_SOFTWARE");
		WebElement eleCreateLeadButton = locateElement("class", "smallSubmit");
		click(eleCreateLeadButton);
		
	}
	

}
