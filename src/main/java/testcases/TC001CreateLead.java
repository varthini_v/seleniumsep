package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import wdMethods.ProjectMethods;


public class TC001CreateLead extends ProjectMethods{

	
	
	@BeforeTest(groups= {"any"})
	public void setTestCaseName()
	{
		testCaseName="TC001CreateLead";
		testCaseDesc="Create a lead";
		author="varthini";
		category="Smoke";
	}
	
	
	@Test(dataProvider= "createLead")//,groups= {"Smoke"},invocationCount=2,timeOut=17000)
	public void createLead_MandatoryFields(String companyName,String fName,String lName,String emailID,String mobileNumber)
	{
		WebElement eleCrmsfaLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmsfaLink);
		WebElement eleCreateLeadLink = locateElement("linktext", "Create Lead");
		click(eleCreateLeadLink);
		WebElement eleCompanyName = locateElement("createLeadForm_companyName");
		type(eleCompanyName, companyName);
		WebElement eleFirstName = locateElement("createLeadForm_firstName");
		type(eleFirstName, fName);
		WebElement eleLastName = locateElement("createLeadForm_lastName");
		type(eleLastName, lName);
		/*WebElement eleIcon = locateElement("xpath", "//img[@src='/images/fieldlookup.gif']");
		click(eleIcon);
		switchToWindow(1);
		WebElement eleAssociateID = locateElement("xpath", "(((//table[@class='x-grid3-row-table'])[1]//tr)[1]/td)[1]//a");
		clickWithoutSnap(eleAssociateID);
		switchToWindow(0);*/
		/*WebElement eleDDIndustry = locateElement("createLeadForm_industryEnumId");
		selectDropDownUsingValue(eleDDIndustry, ddValue);*/
		WebElement emailIdTextBox = locateElement("id", "createLeadForm_primaryEmail");
		type(emailIdTextBox, emailID);
		WebElement phoneNumberTextBox = locateElement("createLeadForm_primaryPhoneNumber");
		type(phoneNumberTextBox, mobileNumber);
		WebElement eleCreateLeadButton = locateElement("class", "smallSubmit");
		click(eleCreateLeadButton);
		
	}
	
@DataProvider(name="createLead",indices= {2})
public Object[][] fetchdata() throws IOException
{
	Object[][] data=LearnReadExcel.readData();
	
	/*Object[][] data=new Object[2][5];
	
	data[0][0]="Cognizant";
	data[0][1]="Varthini";
	data[0][2]="K";
	data[0][3]="varthini.pv@gmail.com";
	data[0][4]="9788153847";
	
	data[1][0]="Cognizant_test";
	data[1][1]="Varthini_test";
	data[1][2]="K_test";
	data[1][3]="varthini.test@gmail.com";
	data[0][4]="9788153846";*/
	
	return data;
	
	
}


/*public void readData() throws IOException
{
	XSSFWorkbook Wb=new XSSFWorkbook("./data/TC001CreateLead");
	XSSFSheet sheet = Wb.getSheetAt(0);
	
	int lastRowNum = sheet.getLastRowNum();
	int lastCellNum = sheet.getRow(0).getLastCellNum();
	
	for(int i=1;i<=lastRowNum;i++)
	{
		XSSFRow row = sheet.getRow(i);
		System.out.print("\n");
		for(int j=0;j<lastCellNum;j++)
		{
			XSSFCell cell = row.getCell(j);
			try {
				String cellValue = cell.getStringCellValue();
				System.out.print("\t"+cellValue);
			} catch (NullPointerException e) {
				System.out.println(" ");
			}
		}
		
		
	}
	
}*/
	
}





