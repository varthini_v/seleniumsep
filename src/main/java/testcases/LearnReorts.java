package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

@Test
public class LearnReorts {
	
	public void report() throws IOException
	{

	ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/Result.html");
	ExtentReports report=new ExtentReports();
	html.setAppendExisting(true);
	report.attachReporter(html);
	ExtentTest logger = report.createTest("TC001CreateLead", "Create a new lead");
logger.log(Status.PASS, "The username is entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
logger.log(Status.PASS, "The password is entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
logger.log(Status.PASS, "The crmsfa link is clicked successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
report.flush();
	}
	
	
}
