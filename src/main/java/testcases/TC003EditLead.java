package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003EditLead extends ProjectMethods {
	
	
	@BeforeTest(groups= {"any"})
	public void setTestCaseName()
	{
		testCaseName="TC003EditLead";
		testCaseDesc="Create a lead";
		author="varthini";
		category="Smoke";
	}
	
	@Test//(groups= {"Regression"},dependsOnGroups= {"Sanity"})
	public void EditLead()
	{
		WebElement eleCrmsfaLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmsfaLink);
		WebElement eleCreateLeadLink = locateElement("linktext", "Create Lead");
		click(eleCreateLeadLink);
		WebElement findLeadsLink = locateElement("linktext", "Find Leads");
		click(findLeadsLink);
		WebElement firstNameTxtBox = locateElement("xpath", "//div[@class='x-panel-body x-panel-body-noheader x-panel-body-noborder']//input[@name='firstName']");
		type(firstNameTxtBox, "Varthini");
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		WebElement tabledata = locateElement("xpath", "((//div[@class='x-grid3-viewport']//tr)[2]/td)[1]//a");
		click(tabledata);
		WebElement editLink = locateElement("linktext", "Edit");
		click(editLink);
		WebElement notesTextBox = locateElement("updateLeadForm_importantNote");
		type(notesTextBox, "Edited");
		WebElement updateButton = locateElement("xpath", "//input[@value='Update']");
		click(updateButton);
		
	}
	

}
