package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002FindLead extends ProjectMethods {
	
	
	@BeforeTest(groups= {"any"})
	public void setTestCaseName()
	{
		testCaseName="TC002FindLead";
		testCaseDesc="Find a lead";
		author="varthini";
		category="Smoke";
	}
	
	
	@Test//(dependsOnMethods= {"testcases.TC001CreateLead.createLead_MandatoryFields"},groups= {"Sanity"},dependsOnGroups= {"Smoke"})
	public void FindLead()
	{
		WebElement eleCrmsfaLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmsfaLink);
		WebElement eleCreateLeadLink = locateElement("linktext", "Create Lead");
		click(eleCreateLeadLink);
		WebElement findLeadsLink = locateElement("linktext", "Find Leads");
		click(findLeadsLink);
		WebElement firstNameTxtBox = locateElement("xpath", "//div[@class='x-panel-body x-panel-body-noheader x-panel-body-noborder']//input[@name='firstName']");
		type(firstNameTxtBox, "Varthini");
		WebElement findLeadsButton = locateElement("xapth", "//button[text()='Find Leads']");
		click(findLeadsButton);
		WebElement table = locateElement("xpath", "//div[@class='x-grid3-viewport']");
		int recordCount=getTableRecordCount(table);
		System.out.println("Number of records retrived for the firstname: "+recordCount);
	}
}
