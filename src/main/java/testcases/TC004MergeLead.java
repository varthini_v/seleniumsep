package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004MergeLead extends ProjectMethods {
	
	@BeforeTest(groups= {"any"})
	public void setTestCaseName()
	{
		testCaseName="TC004MergeLead";
		testCaseDesc="Merge two"
				+ " lead";
		author="varthini";
		category="Smoke";
	}
	@Test
	public void MergeLead()
	{
		WebElement eleCrmsfaLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmsfaLink);
		WebElement eleCreateLeadLink = locateElement("linktext", "Create Lead");
		click(eleCreateLeadLink);
		WebElement mergeLeadsLink = locateElement("linktext", "Merge Leads");
		click(mergeLeadsLink);
		WebElement icon = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(icon);
		switchToWindow(1);
		WebElement firstNameBox = locateElement("xpath", "//input[@names='firstName']");
		type(firstNameBox, "varthini");
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		WebElement tabledata = locateElement("xpath", "((//div[@class='x-grid3-viewport']//tr)[2]/td)[1]//a");
		click(tabledata);
		icon = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(icon);
		switchToWindow(1);
		type(firstNameBox, "varthini");
		click(findLeadsButton);
		tabledata = locateElement("xpath", "((//div[@class='x-grid3-viewport']//tr)[5]/td)[1]//a");
		click(tabledata);
		WebElement mergeLeadsButton = locateElement("class", "buttonDangerous");
		click(mergeLeadsButton);
		acceptAlert();
		takeSnap();
		
	}
	

}
