package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;


public class LearnReadExcel {
	

	public static Object[][] readData() throws IOException
	{
		XSSFWorkbook Wb=new XSSFWorkbook("./data/TC001CreateLead.xlsx");
		XSSFSheet sheet = Wb.getSheetAt(0);
		
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		Object data[][]=new Object[lastRowNum][lastCellNum];
			
		for(int i=1;i<=lastRowNum;i++)
		{
			XSSFRow row = sheet.getRow(i);
			System.out.print("\n");
			for(int j=0;j<lastCellNum;j++)
			{
				XSSFCell cell = row.getCell(j);
				try {
					data[i-1][j] = cell.getStringCellValue();
				} catch (NullPointerException e) {
					System.out.println(" ");
				}
			}
			
		
		}
		Wb.close();
		return data;	
	}

}
