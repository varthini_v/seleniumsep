package week1.day2;

import java.util.Scanner;

public class SumOfOddDigit {

	public static void main(String[] args) {
		int size;
		Scanner Scan=new Scanner(System.in);
	System.out.println("Enter the number of digits in the number");
	size=Scan.nextInt();
		int[] Number=new int[size];
		int sum=0;
		
		System.out.println("Enter the Number");
		for(int i=0;i<Number.length;i++) 
		{
		Number[i]=Scan.nextInt();	
		}
		Scan.close();
		for(int i=0;i<Number.length;i++)
		{
			if((i%2)==0)
			{
				sum=sum+Number[i];
			}
		}
		System.out.println("The Sum of odd digits in the number: "+sum);
	}

}
