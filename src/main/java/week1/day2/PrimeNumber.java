package week1.day2;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		int Number,flag=0;
		System.out.println("Enter the number to be verified");
		Scanner Scan=new Scanner(System.in);
		Number=Scan.nextInt();
		Scan.close();
		for(int i=2;i<=(Number/2);i++)
		{
			if((Number%i)==0)
			{
				flag=1;
				break;
			}
		}
		if(flag==1)
		{
			System.out.println("The given number is not a prime number");
		}
		else if(flag==0)
		{
			System.out.println("The given number is a prime number");
		}

	}

}
