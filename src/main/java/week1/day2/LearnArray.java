package week1.day2;
import java.util.Scanner;
public class LearnArray 
{
	public static void main(String[] args) 
	{
		Scanner Scan=new Scanner(System.in);
		int Sum=0;
		System.out.println("Enter the count of numbers");
		int size=Scan.nextInt();
		int[] Numbers=new int[size];
		System.out.println("Enter the numbers:");
		for(int i=0;i<Numbers.length;i++)
		{
			Numbers[i]=Scan.nextInt();
		}
		Scan.close();
		for(int eachNumber:Numbers)
		{
			if((eachNumber%2)!=0)
			{
				Sum=Sum+eachNumber;
			}
		}
		System.out.println("Sum of odd numbers in the array= "+Sum);
	}

}
