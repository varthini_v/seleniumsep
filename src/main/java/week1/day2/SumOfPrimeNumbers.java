package week1.day2;

import java.util.Scanner;

public class SumOfPrimeNumbers {

	public static void main(String[] args) {
		int maxCount;
		int sum=0;
		
		System.out.println("Enter the count of numbers:");
		Scanner Scan=new Scanner(System.in);
		maxCount=Scan.nextInt();
		Scan.close();
		for(int i=2;i<=maxCount;i++)
		{
			boolean flag=true;
			
			for(int j=2;j<=i/2;j++)
			{
				if((i%j)==0)
				{
					flag=false;
					break;
					
				}
			}
			
			if(flag==true)
			{
				sum=sum+i;
			}
		}
System.out.println("The Sum of prime Numbers:"+sum);
	}

}
