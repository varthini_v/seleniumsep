package dailyexercisesJava;

import java.util.Scanner;

public class CC011PercentageOfCharacters {

	public static void main(String[] args) {
	String inputString;
	System.out.println("Enter the String to be verified");
	Scanner scan=new Scanner(System.in);
	inputString=scan.nextLine();
	int upperCaseCount=0,lowerCaseCount=0,digitCount=0,otherCharcterCount=0;
	int stringLength=inputString.length();
	for(int charCount=0;charCount<stringLength;charCount++)
	{   String tempString="";
		tempString=tempString+inputString.charAt(charCount);
		
		if(tempString.matches("[A-Z]"))
		{
			upperCaseCount++;
		}
		else if(tempString.matches("[a-z]"))
		{
			lowerCaseCount++;
		}
		else if(tempString.matches("[0-9]"))
		{
			digitCount++;
		}
		else if(tempString.matches("[\\W]"))
		{
			otherCharcterCount++;
		}
		
	}   
		
		float upperCaseAvg=(upperCaseCount*100)/stringLength;
		float lowerCaseAvg=(lowerCaseCount*100)/stringLength;
		float digitAvg=(digitCount*100)/stringLength;
		float otherCharacterAvg=(otherCharcterCount*100)/stringLength;
		
		System.out.println("Number of Upper Case Letters :"+upperCaseCount+ ", So the percentage is  "+upperCaseAvg);
		System.out.println("Number of Lower Case Letters :"+lowerCaseCount+ ", So the percentage is "+lowerCaseAvg);
		System.out.println("Number of Digits             :"+digitCount+", So the percentage is "+digitAvg);
		System.out.println("Number of Other Characters   :"+otherCharcterCount+" So the percentage is "+otherCharacterAvg);
		
 scan.close();
	}





}