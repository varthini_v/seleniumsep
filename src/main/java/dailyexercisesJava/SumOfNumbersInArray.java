package dailyexercisesJava;


import java.util.Scanner;

public class SumOfNumbersInArray {
	public static void main(String[] args) {
	int size,sum=0;
	Scanner scan=new Scanner(System.in);
	System.out.println("Enter the array limit");
	size=scan.nextInt();
	int[] Numbers=new int[size];
	System.out.println("Enter "+size+" Numbers");
	for(int i=0;i<size;i++)
	{
		Numbers[i]=scan.nextInt();
		sum=sum+Numbers[i];
		
	}
	scan.close();
	System.out.println("Sum of the Numbers in an array: "+sum);
	}

}
