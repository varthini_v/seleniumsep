package dailyexercisesJava;

public class CC05AmastrongNumber {
	
	static int sum=0;
	
	public int sumofcubes(int digit)
	{
		
		sum=sum+(digit*digit*digit);
		return sum;
	}

	public static void main(String[] args) {
		int digit=0,remainder=0;
		CC05AmastrongNumber object= new CC05AmastrongNumber();
		System.out.println("Amstrong Numbers between 100 and 1000");
		//int number=153;
		for( int number=100;number<1000;number++)//Getting digits of the number and sum their cubes
		{
        	//System.out.println(number+"number");
			digit=number/100;// digit in 100th position
        	remainder=number%100;
        	sum=object.sumofcubes(digit);
        	digit=remainder/10;//digit in 10th position
        	remainder=remainder%10;//digit in 1st position
        	sum=object.sumofcubes(digit);
        	sum=object.sumofcubes(remainder);
        	if(sum==number)// check if the sum of digits equal to the number 
        	{
        		System.out.println(number);
        	}
            sum=0;
		}
        
		
		
	}

}
