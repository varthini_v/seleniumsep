package dailyexercisesJava;


import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class CC018_PrintUniqueCharacters {

/*	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the String:");
		String inputString=scan.nextLine();
		scan.close();
		Set<Character> arrayOfInputString=new LinkedHashSet<>();
		for(int i=0;i<inputString.length();i++)
		{
			arrayOfInputString.add(inputString.charAt(i));
		}
		for(char eachChar:arrayOfInputString)
		{
			if(eachChar!=' ')
			System.out.print(Character.toLowerCase(eachChar));
		}

	}

}
*/


/*package dailyprogram;

public class Code_18{ */ 

	public static void main(String[] args) {
		/*Write a simple Java program to identify and form a string with unique characters. 
		Ex. Input=Good Looking. 
			Output should be = godlkin.*/
		String input="Good looking";
		String output="";
		char b[]=input.toLowerCase().replace(" ", "").toCharArray();
		System.out.println("Input:"+input);
		for (int i=0;i<b.length;i++) {
			if(!output.contains(b[i]+"")) {
				output = output + b[i];
			}
		}
		/*for (int i=0;i<b.length;i++) {
			if(output.indexOf(b[i]) == -1) {
				output = output + b[i];
			}
		}*/
		System.out.println("Output:"+output);
	}

}
