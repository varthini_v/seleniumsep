package dailyexercisesJava;

import java.util.Scanner;

public class PatternPrinting {

	public static void main(String[] args) 
	{
		int firstNumber,lastNumber,i;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the Starting Number:");
		firstNumber=scan.nextInt();
		System.out.println("Enter the Ending Number:");
		lastNumber=scan.nextInt();
		scan.close();
		System.out.println(" ");
		for(i=firstNumber;i<=lastNumber;i++)
		{
			if((i%3==0)&&(i%5==0))
			{
				System.out.print("FIZZBUZZ\t");
			}
			else if(i%3==0)
			{
				System.out.print("FIZZ\t");
			}
			else if(i%5==0)
			{
				System.out.print("BUZZ\t");
			}
			else
			{
				System.out.print(i+"\t");
			}
		}
		
	}
}
