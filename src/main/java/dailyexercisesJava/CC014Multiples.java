package dailyexercisesJava;

public class CC014Multiples {

	public static void main(String[] args) {
		int sum=0;
		System.out.println("Multiples of 3 and 5, less than 100: ");
		for(int i=1;i<100;i++)
		{
			if((i%3==0)||(i%5==0))
			{
				System.out.print(i+"\t");
				sum=sum+i;
			}
		}
		
		System.out.println("\nThe sum of multiples of 3 and 5,less than 100: "+sum);
	}
}
