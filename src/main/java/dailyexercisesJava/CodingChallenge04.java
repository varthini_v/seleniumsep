package dailyexercisesJava;

import java.util.Scanner;

public class CodingChallenge04
{
	public static void main(String[] args) 
	{	
		StringBuilder inputWord=new StringBuilder();
		StringBuilder reversedWord=new StringBuilder();
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the word to be chacked: ");
		inputWord.append(scan.nextLine());
		scan.close();
		String inputStr=inputWord.toString();
		reversedWord=inputWord.reverse();
		System.out.println(reversedWord);
		String reverseStr=reversedWord.toString();
		System.out.println(inputStr);
		System.out.println(reverseStr);
		if(inputStr.equalsIgnoreCase(reverseStr))
		{
			System.out.println("The entered word is a palindrome");
		}
		else
		{
			System.out.println("The entered word is not a palindrome");
		}
		
	
	
	}
}
