package dailyexercisesJava;

import java.util.Scanner;

public class CC07LeapYear
{
	
	public static void main(String[] args)
	{
		int inputYear;
		boolean flag;
		System.out.println("Enter the year to be verified:");
		Scanner scan=new Scanner(System.in);
		inputYear=scan.nextInt();
		if(inputYear%100==0)
		{
			if(inputYear%400==0)
			{
				System.out.println("The given year "+inputYear+" is a leap year");
			}
			else
			{
				System.out.println("The given year "+inputYear+" is  not a leap year");

			}
		}
		else if(inputYear%4==0)
		{
			System.out.println("The given year "+inputYear+" is a leap year");
		}
		else
		{
			System.out.println("The given year "+inputYear+" is  not a leap year");

		}
scan.close();
	}
	

}
