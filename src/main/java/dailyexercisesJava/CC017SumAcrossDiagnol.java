package dailyexercisesJava;

import java.util.Scanner;

import org.apache.poi.ss.usermodel.Row;

public class CC017SumAcrossDiagnol 
{

	public static void main(String[] args) 
	{
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the limit for nxn array:");
		int size=scan.nextInt();
		int[][] numbers=new int[size][size];
		System.out.println("Enter the array elements in row wise:");
		for(int i=0;i<size;i++) //to get the array of numbers
		{
			for(int j=0;j<size;j++)
			{
				numbers[i][j]=scan.nextInt();
			}
		}
		scan.close();
		System.out.println("The entered "+size+"x"+size+" array : ");
		for(int i=0;i<size;i++)//to print the array
		{
			System.out.println("");
			for(int j=0;j<size;j++)
			{
				System.out.print(numbers[i][j]+"\t");
			}
		}
		
		int sumAcrossUpperDiagnol=0;
		int diagnol=size-1;
		for(int i=0;i<(size-1);i++)
		{
			diagnol--;
			for(int j=0;j<=diagnol;j++)
			{
				sumAcrossUpperDiagnol=sumAcrossUpperDiagnol+numbers[i][j];
			}


		}
		System.out.println("\nsumAcrossUpperDiagnol:"+sumAcrossUpperDiagnol);
		diagnol=0;
		int sumAcrossLowerDiagnol=0;
		for(int i=(size-1);i>0;i--)
		{
			diagnol++;
			for(int j=diagnol;j<=(size-1);j++)
			{
				sumAcrossLowerDiagnol=sumAcrossLowerDiagnol+numbers[i][j];
			}
	

		}

		System.out.println("sumAcrossLowerDiagnol: "+sumAcrossLowerDiagnol);
		System.out.println("The numbers which has greatest sum across diagnol:");
		//Compare the sum across diagonal and print the numbers which has greatest sum across the diagonal
		if (sumAcrossLowerDiagnol>sumAcrossUpperDiagnol)
		{
		
			diagnol=0;
			for(int i=(size-1);i>0;i--)
			{
				diagnol++;
				for(int j=diagnol;j<=(size-1);j++)
				{
					System.out.print(numbers[i][j]+"\t");
				}
		

			}
		}
		else
		{   
			diagnol=size-1;
			for(int i=0;i<size-1;i++)
			{
				diagnol--;
				for(int j=0;j<=diagnol;j++)
				{
					System.out.print(numbers[i][j]+"\t");
				}
			}
		
		}
	}
}