package dailyexercisesJava;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class CC06RepeatedNumbers
{

public static void main(String[] args)
	{
		int size;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the array Size");
		size=scan.nextInt();
		int[] numbers=new int[size];
		Set<Integer> duplicates=new LinkedHashSet<>();
		System.out.println("Enter the array elements");
		for(int i=0;i<size;i++) 
		{
			numbers[i]=scan.nextInt();
			
		}
		for(int i=0;i<size;i++)
		{
			for(int j=i+1;j<size;j++)
			{
				if(numbers[i]==numbers[j])
				{
					duplicates.add(numbers[i]);
				}
					
				}
			}
		
		System.out.println("Duplicates in the array");
		for(int eachNumber:duplicates)
		{
			System.out.println(eachNumber);
		}
		scan.close();
		
	}	
	
}
