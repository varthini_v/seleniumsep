package dailyexercisesJava;

import java.util.Scanner;

public class ArithmeticOperations
{

	public static void main(String[] args)
	{
		System.out.println("Enter 2 numbers");
		Scanner scan=new Scanner(System.in);
		int Value1 = scan.nextInt();
		int Value2 = scan.nextInt();
		System.out.println("Enter any one option Add//Subract//Multiply//Divide");
		String Value3 = scan.next();
		scan.close();
		try
		{
			
		
			switch (Value3)
			{
				case "Add":
					System.out.println("Added\nAnswer "+(Value1+Value2));
					break;
				case "Subract":
					System.out.println("Subracted\nAnswer "+(Value1-Value2));
					break;
				case "Multiply":
					System.out.println("Multiplied\nAnswer "+(Value1*Value2));
					break;
				case "Divide":
					System.out.println("Divided\nAnswer "+(Value1/Value2));
					break;
				default:
					System.out.println("Invalid Input");
					break;
			}
		}
		catch(ArithmeticException e) 
		{
			System.out.println("Operation Cannot be performed on the above Numbers");
		}
		
	}

}
