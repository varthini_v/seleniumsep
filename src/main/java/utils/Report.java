package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class Report {
	public String testCaseName;
	public String testCaseDesc;
	public 	String author;
	public String category;
	public int dataSheet;
	public String workBook;
	public String name;

	
	public static ExtentHtmlReporter report;
	public static ExtentReports reportfinal;
	public static ExtentTest logger;
	
	
	public void startReport()
	{
		report=new ExtentHtmlReporter("./reports/result.html");
		report.setAppendExisting(false);
		reportfinal=new ExtentReports();
		reportfinal.attachReporter(report);
		
	}
	public void reportName(/*String testCaseName,String testCaseDesc,String author,String category*/)
	{
		logger = reportfinal.createTest(testCaseName, testCaseDesc);
		logger.assignAuthor(author);
		logger.assignCategory(category);
	}
	
	public void reportName(String testCaseName,String testCaseDesc,String author,String category)
	{
		logger = reportfinal.createTest(testCaseName, testCaseDesc);
		logger.assignAuthor(author);
		logger.assignCategory(category);
	}
	
	
	
	
	public void testStep(String status,String Description)
	{
		if(status.equalsIgnoreCase("pass"))
		{
			logger.log(Status.PASS, Description);
		}
		else if(status.equalsIgnoreCase("fail"))
		{
			logger.log(Status.FAIL, Description);
		}
	}
	
	
	public void endResult()
	{
		reportfinal.flush();
	}
}


