package assesment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MobileSearch 
{	
	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
		WebElement searchbar = driver.findElementByClassName("LM6RPg");
		searchbar.click();
		searchbar.sendKeys("iphone x",Keys.ENTER);
		String price = driver.findElementByXPath("(//div[@class='_1vC4OE'])[2]").getText();
		System.out.println("The price of second listed mobile: "+price);
	}

}
